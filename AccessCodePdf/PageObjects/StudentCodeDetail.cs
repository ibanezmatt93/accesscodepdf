﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using AccessCodePdf.Helpers;
using AccessCodePdf.Models;
using SkiaSharp;
using static AccessCodePdf.Models.NineBoxPosition;

namespace AccessCodePdf.PageObjects
{
    public class StudentCodeDetail
    {
        private SKPaint labelStyle;
        private SKPaint detailStyle;
        private SKPaint accessCodeStyle;
       
        public StudentCodeDetail(SKCanvas pageCanvas, float xOffset, float yOffset, StudentAccessCodeForPdf studentAccessCodeForPdf, float fontSize)
        {
            Page = pageCanvas;
            XOffset = xOffset;
            YOffset = yOffset;
            Student = studentAccessCodeForPdf;
            FontSize = fontSize;

            labelStyle = PaintStyle.GetTextStyle(Color.Gray, FontSize, SKTextAlign.Left, false);
            detailStyle = PaintStyle.GetTextStyle(Color.MediumPurple, FontSize, SKTextAlign.Left, false);
            accessCodeStyle = PaintStyle.GetTextStyle(Color.MediumPurple, 3 * FontSize, SKTextAlign.Left, false);

        }

        public float FontSize { get; set; }

        public SKCanvas Page { get; private set; }

        public StudentAccessCodeForPdf Student { get; private set; }

        public float XOffset { get; private set; }

        public float YOffset { get; private set; }

        public void Generate(Position position)
        {
            const float lineSpacing = 12f;
            const float wordSpacing = lineSpacing / 2;
            SKRect labelRect = new SKRect();
            SKRect detailRect = new SKRect();

            // Student Name
            labelStyle.MeasureText("Name :", ref labelRect);
            Page.DrawText("Name :",XOffset, YOffset, labelStyle);

            detailStyle.MeasureText(Student.Name, ref detailRect);            
            Page.DrawText(Student.Name, XOffset + labelRect.Width + wordSpacing, YOffset, detailStyle);


            // Access Code
            accessCodeStyle.MeasureText(Student.AccessCode, ref detailRect);
            YOffset += detailRect.Height + lineSpacing;
            Page.DrawText(Student.AccessCode, XOffset , YOffset, accessCodeStyle);


            //TestName
            labelStyle.MeasureText("Test :", ref labelRect);
            YOffset += labelRect.Height + lineSpacing;
            Page.DrawText("Test :", XOffset, YOffset, labelStyle);

            detailStyle.MeasureText(Student.TestName, ref detailRect);
            Page.DrawText(Student.TestName, XOffset + labelRect.Width + wordSpacing, YOffset, detailStyle);


            // Sitting Name
            labelStyle.MeasureText("Sitting :", ref labelRect);
            YOffset += labelRect.Height + lineSpacing;
            Page.DrawText("Sitting :", XOffset, YOffset, labelStyle);

            detailStyle.MeasureText(Student.SittingName, ref detailRect);
            Page.DrawText(Student.SittingName, XOffset + labelRect.Width + wordSpacing, YOffset, detailStyle);


            // Start Datetime
            labelStyle.MeasureText("Date :", ref labelRect);
            YOffset += labelRect.Height + lineSpacing;
            Page.DrawText("Date :", XOffset, YOffset, labelStyle);

            detailStyle.MeasureText(Student.SittingStartDate.ToString("dd MMM yyyy, HH:mm"), ref detailRect);
            Page.DrawText(Student.SittingStartDate.ToString("dd MMM yyyy, HH:mm"), XOffset + labelRect.Width + wordSpacing, YOffset, detailStyle);


            // URL
            labelStyle.MeasureText("Url :", ref labelRect);
            YOffset += labelRect.Height + lineSpacing;
            Page.DrawText("Url :", XOffset, YOffset, labelStyle);

            detailStyle.MeasureText(Student.TestLobbyUrl, ref detailRect);
            Page.DrawText(Student.TestLobbyUrl, XOffset + labelRect.Width + wordSpacing, YOffset, detailStyle);

            // Build the box

            switch (position)
            {
                case Position.TOPLEFT:
                    break;
                case Position.TOPCENTRE:
                case Position.TOPRIGHT:
                    break;
                case Position.CENTRELEFT:
                case Position.BOTTOMLEFT:
                    break;
                case Position.CENTRE:
                case Position.CENTRERIGHT:
                case Position.BOTTOMCENTRE:
                case Position.BOTTOMRIGHT:
                    break;
            }

        }
    }
}
