﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using AccessCodePdf.Helpers;
using AccessCodePdf.Models;
using SkiaSharp;

namespace AccessCodePdf.PageObjects
{
    public class DashedGrid
    {
        public DashedGrid(SKCanvas pageCanvas, SKPaint paintStyle)
        {
            Page = pageCanvas;
            PaintStyle = paintStyle;
        }

        private SKCanvas Page { get; set; }

        private SKPaint PaintStyle { get; set; }
        
        public void Generate(float pageHeight, float pageWidth, PageMargins pageMargins)
        {
            // Create dashed rectangle
            var rectWidth = pageWidth - (2 * pageMargins.Vertical);
            var rectHeight = pageHeight - (2 * pageMargins.Horizontal);
            Page.DrawRect(pageMargins.Horizontal, pageMargins.Vertical, rectWidth, rectHeight, PaintStyle);
            GenerateVerticalLines(pageHeight, pageWidth, pageMargins);
            GenerateHorizontalLines(pageHeight, pageWidth, pageMargins);
        }

        private void GenerateVerticalLines(float pageHeight, float pageWidth, PageMargins pageMargins)
        {
            var lineX0 = pageMargins.Horizontal + (pageWidth - (2 * pageMargins.Vertical)) / 3;
            var lineY0 = pageMargins.Horizontal;
            var lineX1 = lineX0;
            var lineY1 = pageHeight - pageMargins.Horizontal;

            Page.DrawLine(lineX0, lineY0, lineX1, lineY1, PaintStyle);

            lineX0 = pageMargins.Horizontal + 2 * ((pageWidth - (2 * pageMargins.Vertical)) / 3);
            lineY0 = pageMargins.Horizontal;
            lineX1 = lineX0;
            lineY1 = pageHeight - pageMargins.Horizontal;

            Page.DrawLine(lineX0, lineY0, lineX1, lineY1, PaintStyle);
        }

        private void GenerateHorizontalLines(float pageHeight, float pageWidth, PageMargins pageMargins)
        {
            var lineY0 = pageMargins.Vertical + (pageHeight - (2 * pageMargins.Horizontal)) / 3;
            var lineX0 = pageMargins.Vertical;
            var lineY1 = lineY0;
            var lineX1 = pageWidth - pageMargins.Vertical;

            Page.DrawLine(lineX0, lineY0, lineX1, lineY1, PaintStyle);

            lineY0 = pageMargins.Vertical + 2 * ((pageHeight - (2 * pageMargins.Horizontal)) / 3);
            lineX0 = pageMargins.Vertical;
            lineY1 = lineY0; Page.DrawLine(lineX0, lineY0, lineX1, lineY1, PaintStyle);
            lineX1 = pageWidth - pageMargins.Vertical;

            Page.DrawLine(lineX0, lineY0, lineX1, lineY1, PaintStyle);
        }
    }
}
