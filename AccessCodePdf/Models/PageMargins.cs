﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessCodePdf.Models
{
    public class PageMargins
    {
        public PageMargins(float horizontalMargin, float verticalMargin)
        {
            Horizontal = horizontalMargin;
            Vertical = verticalMargin;
        }

        public float Horizontal { get; private set; }

        public float Vertical { get; private set; }
    }
}
