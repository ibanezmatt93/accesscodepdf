﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessCodePdf.Models
{
    public class PointLocation
    {
        public (float X, float Y) Location { get; private set; }

        public PointLocation(float x, float y)
        {
            Location = (x, y);
        }
    }
}
