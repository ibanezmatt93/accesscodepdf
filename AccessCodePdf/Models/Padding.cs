﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessCodePdf.Models
{
    public class Padding
    {
        public Padding(float horizontalPadding, float verticalPadding)
        {
            HorizontalPadding = horizontalPadding;
            VerticalPadding = verticalPadding;
        }

        public float HorizontalPadding { get; private set; }

        public float VerticalPadding { get; private set; }
    }
}
