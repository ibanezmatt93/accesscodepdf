﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessCodePdf.Models
{
    public class PageDimensions
    {
        public float Width { get; private set; }

        public float Height { get; private set; }

        public PageDimensions(float width, float height)
        {
            Width = width;
            Height = height;
        }
    }
}
