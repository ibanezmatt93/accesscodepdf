﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessCodePdf.Models
{
    public class StudentAccessCodeForPdf
    {
        public string Name { get; set; }

        public string AccessCode { get; set; }

        public string SittingName { get; set; }

        public DateTime SittingStartDate { get; set; }

        public string TestName { get; set; }

        public string TestLobbyUrl { get; set; }
    }
}
