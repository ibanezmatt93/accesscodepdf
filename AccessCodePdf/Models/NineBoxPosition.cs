﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessCodePdf.Models
{
    public static class NineBoxPosition
    {
        public enum Position
        {
            TOPLEFT = 0,
            TOPCENTRE,
            TOPRIGHT,
            CENTRELEFT,
            CENTRE,
            CENTRERIGHT,
            BOTTOMLEFT,
            BOTTOMCENTRE,
            BOTTOMRIGHT
        }
    }
}
