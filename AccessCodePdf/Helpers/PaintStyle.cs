﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using SkiaSharp;

namespace AccessCodePdf.Helpers
{
    public static class PaintStyle
    {
        public static SKPaint GetDashedStyle(Color lineColor, float strokeWidth)
        {
            float[] dashArray = new float[2] { 4, 2 };

            return new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                StrokeWidth = strokeWidth,
                Color = new SKColor(lineColor.R, lineColor.G, lineColor.B),
                PathEffect = SKPathEffect.CreateDash(dashArray, 0)
            };
        }

        public static SKPaint GetTextStyle(Color textColor, float textSize, SKTextAlign textAlign, bool isStroke )
        {
            return new SKPaint()
            {
                TextSize = textSize,
                Color = new SKColor(textColor.R, textColor.G, textColor.B),
                TextAlign = textAlign,
                IsStroke = isStroke
            };
        }
    }
}
