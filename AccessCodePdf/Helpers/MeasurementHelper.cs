﻿using System;
using System.Collections.Generic;
using System.Text;
using AccessCodePdf.Models;

namespace AccessCodePdf.Helpers
{
    static class MeasurementHelper
    {
        // 1 DPI
        private static float pxPerMm = 0.03937008f;

        public static PageDimensions GetHeightAndWidthInPixels(float heightInMm, float widthInMm, float dpi)
        {
            float pixelsPerMm = dpi * pxPerMm;

            return new PageDimensions(widthInMm * pixelsPerMm, heightInMm * pixelsPerMm);
        }

        public static float ConvertMmToPixel(float mm, float dpi)
            => mm * dpi * pxPerMm;
    }
}
