﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using AccessCodePdf.Helpers;
using AccessCodePdf.Models;
using AccessCodePdf.PageObjects;
using SkiaSharp;

namespace AccessCodePdf
{
    static class Program
    {
        private static Random _rand = new Random();
        private static IEnumerable<StudentAccessCodeForPdf> students = new List<StudentAccessCodeForPdf>()
        {
            new StudentAccessCodeForPdf()
            {
                AccessCode = "acac-acac",
                Name = "Student Name",
                SittingName = "Sitting Name",
                SittingStartDate = DateTime.Now,
                TestLobbyUrl = "TestLobbyUrl",
                TestName = "Test Name"
            }
        };

        static void Main(string[] args)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                SKDocumentPdfMetadata pdfMetadata = new SKDocumentPdfMetadata(300);
                using (SKDocument document = SKDocument.CreatePdf(stream, pdfMetadata))
                {
                    var margin = MeasurementHelper.ConvertMmToPixel(12.7f, pdfMetadata.RasterDpi);
                    var padding = MeasurementHelper.ConvertMmToPixel(5f, pdfMetadata.RasterDpi);
                    PageMargins pageMargins = new PageMargins(margin, margin);
                    Padding dataMargin = new Padding(padding, padding * 2);
                    PageDimensions measurements = MeasurementHelper.GetHeightAndWidthInPixels(210, 297, pdfMetadata.RasterDpi);
                    SKCanvas page = document.BeginPage(measurements.Width, measurements.Height);

                    new DashedGrid(page, PaintStyle.GetDashedStyle(Color.LightGray, 2f)).Generate(measurements.Height, measurements.Width, pageMargins);

                    foreach (var student in students)
                    {

                        new StudentCodeDetail(page, 
                            pageMargins.Horizontal + dataMargin.HorizontalPadding, 
                            pageMargins.Vertical + dataMargin.VerticalPadding, 
                            student,
                            14f
                            ).Generate(0);
                                                
                    }

                    document.EndPage();
                }

                File.WriteAllBytes($"C:/Temp/SkiaSharp{_rand.Next()}.pdf", stream.ToArray());
            }
        }
    }
}
