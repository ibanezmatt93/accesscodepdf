﻿using System;
using System.Collections.Generic;
using System.Text;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using PdfSharpCore.Helpers;

namespace PdfSharpCore.PageObjects
{
    public static class AccessCodeTable
    {
        public static Table Generate(Section sec, int studentCount)
        {
            var totalElementWidth = sec.PageSetup.PageWidth - (sec.PageSetup.LeftMargin + sec.PageSetup.RightMargin);
            float elementWidth;
            switch (studentCount)
            {
                case 1:
                    elementWidth = totalElementWidth / 3;
                    break;
                case 2:
                    elementWidth = (totalElementWidth / 3) * 2;
                    break;
                case 3:
                    elementWidth = totalElementWidth;
                    break;
            }

            Table tab = sec.AddTable();

            for (int i = 0; i < studentCount; i++)
            {
                Column column = tab.AddColumn(totalElementWidth / 3);
                column.Format.Alignment = ParagraphAlignment.Left;
            }

            TableHelpers.AddSingleCellRow(tab, "Name", "Jimberb");

            return tab;
        }
    }
}
