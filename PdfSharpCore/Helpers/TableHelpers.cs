﻿using System;
using System.Collections.Generic;
using System.Text;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;

namespace PdfSharpCore.Helpers
{
    public static class TableHelpers
    {
        public static void CreateHeaderRow(Table table, List<string> headers, Color rowColour)
        {
            if (table.Columns.Count < headers.Count)
            {
                throw new ArgumentException("You have provided more headers than we have available columns.");
            }
            if (table.Columns.Count > headers.Count)
            {
                throw new ArgumentException("You have more columns than headers, please provide the correct number of headers.");
            }

            Row row = table.AddRow();
            row.TopPadding = 4;
            row.BottomPadding = 2.5;
            row.Shading.Color = rowColour;
            row.Format.Font = new Font("Arial", 9);
            row.Format.Font.Color = Colors.Black;
            row.Format.Font.Bold = true;

            for (int i = 0; i < table.Columns.Count; i++)
            {
                Cell cell = row.Cells[i];
                cell.AddParagraph(headers[i]);
            }
        }

        public static void AddSingleCellRow(Table table, string label, string value)
        {
            Row row = table.AddRow();
            row.TopPadding = 2.5;
            row.BottomPadding = 2.5;
            row.Format.Font = new Font("Arial", 9);
            Cell cell = row.Cells[0];
            if (table.Columns.Count > 1)
            {
                cell.MergeRight = table.Columns.Count - 1;
            }
            Paragraph para = cell.AddParagraph();
            para.AddFormattedText($"{label}: ", TextFormat.Bold).Color = ColourConverter.GetColorFromRGB(29, 193, 233);
            para.AddFormattedText(value).Color = ColourConverter.GetColorFromRGB(0, 0, 0);
        }

        public static void AddMultiCellRow(Table table, int noOfCells, List<Dictionary<string, TextFormat>> labels, List<string> values)
        {
            Row row = table.AddRow();
            row.TopPadding = 2.5;
            row.BottomPadding = 2.5;
            row.Format.Font = new Font("Arial", 9);
            row.Format.Font.Bold = true;
            for (int i = 0; i < noOfCells; i++)
            {
                Cell cell = row.Cells[i];
                Paragraph para = cell.AddParagraph();
                foreach (var label in labels[i])
                {
                    para.AddFormattedText($"{label.Key}", label.Value).Color = ColourConverter.GetColorFromRGB(29, 193, 233);
                }
                para.AddFormattedText(values[i], TextFormat.NotBold).Color = ColourConverter.GetColorFromRGB(0, 0, 0);
            }
        }
    }
}
