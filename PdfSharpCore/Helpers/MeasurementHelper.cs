﻿using System;
using System.Collections.Generic;
using System.Text;
using PdfSharp.Drawing;

namespace PdfSharpCore.Helpers
{
    public static class MeasurementHelper
    {
        public static double GetPointsFromMm(double mm, XUnit sizeObject)
            => sizeObject.Point * (mm / sizeObject.Millimeter);
    }
}
