﻿using System.Drawing;
using md = MigraDoc.DocumentObjectModel;

namespace PdfSharpCore.Helpers
{
    public static class ColourConverter
    {
        public static md.Color GetColorFromHexValue(string hexCode)
        {
            var colour = ColorTranslator.FromHtml(hexCode);
            return new md.Color(colour.A, colour.R, colour.G, colour.B);
        }

        public static md.Color GetColorFromRGB(int r, int g, int b)
        {
            Color colour = Color.FromArgb(r, g, b);
            return new md.Color(colour.A, colour.R, colour.G, colour.B);
        }
    }
}
